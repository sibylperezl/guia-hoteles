$(function () {
  $("[data-toggle='popover']").popover();
  $("[data-toggle='tooltip']").tooltip();
  $('.carousel').carousel({
    interval: 2000
  });

  $('#contacto').on('show.bs.modal', function (e){ 
    console.log('El modal contacto se esta mostrando');
    
    $('#contactoBtn').removeClass('btn-outline-primary');
    $('#contactoBtn').addClass('btn-outline-success');
    $('#contactoBtn').prop('disable', true);

  });
  $('#contacto').on('shown.bs.modal', function (e){ 
    console.log('El modal contacto se mostró');
  });
  $('#contacto').on('hide.bs.modal', function (e){ 
    console.log('El modal contacto se oculta');
  });
  $('#contacto').on('hidden.bs.modal', function (e){ 
    console.log('El modal contacto se ocultó');
    $('#contactoBtn').prop('disable', false);
    $('#contactoBtn').removeClass('btn-outline-success');
    $('#contactoBtn').addClass('btn-outline-primary');
  });
});
